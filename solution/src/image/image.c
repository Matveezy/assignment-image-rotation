#include "image.h"

/**
 * Метод, создающий структуры картинки с заданными длиной и шириной, аллоцируем память под пиксели
 * @param width
 * @param height
 * @return
 */
struct image create_image (size_t const width , size_t const height){
    return (struct image) {.width = width , .height = height , .data = malloc(width * height * sizeof(struct pixel))};
}


/**
 * Метод, освобождающий аллоцированную память
 * @param img
 */
void image_delete(struct image img){
    free(img.data);
}


