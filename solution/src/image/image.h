#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdlib.h>
//#include <malloc.h>
#include <stddef.h>
#include <stdint.h>

#pragma pack(push, 1)
/**
 * Структура , в которой мы будем хранить наше изобрежения
 * Состоит из значений ширины и высоты , а также массива структур пикселей
 */
struct image {
    size_t width, height;
    struct pixel *data;
};
#pragma pack(pop)

struct pixel {
    uint8_t b, g, r;
};

struct image create_image(size_t const width, size_t const height);

void image_delete(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H


