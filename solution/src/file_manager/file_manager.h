//
// Created by Matthew on 23.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
#include "stdio.h"
#define ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
enum state {
    EXCELLENT = 0,
    OPEN_MODE_ERROR,
    OPEN_ERROR,
    CLOSE_ERROR,
    READ_ERROR,
    WRITE_ERROR,
    CONVERT_ERROR
};

struct file {
    enum state state;
    FILE *file;
};

struct file file_open(const char* file_name);

struct file file_output_open(const char* file_name);

enum state file_close(FILE *file);


#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
