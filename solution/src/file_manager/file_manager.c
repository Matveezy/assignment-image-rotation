#include "file_manager.h"
#include <stdio.h>
#include <string.h>


/**
 * Метод, открывающий файл с правами на чтение
 * @param file_name
 * @return
 */
struct file file_open(const char* file_name) {
    FILE *file;
    if ((file = fopen(file_name, "r")) == NULL) return (struct file) {.state=OPEN_MODE_ERROR, .file = NULL};
    return (struct file) {.state=EXCELLENT, .file=file};
}

/**
 * Метод, открывающий файл на запись(отличается правами)
 * @param file_name
 * @return
 */
struct file file_output_open(const char* file_name) {
    FILE *file;
    if ((file = fopen(file_name, "w")) == NULL) return (struct file) {.state = OPEN_MODE_ERROR, .file = NULL};
    return (struct file) {.file=file, .state =EXCELLENT};
}

/**
 * Метод, закрывающий поток , открытый с помощью fopen
 * @param file
 * @return
 */
enum state file_close(FILE *file) {
    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    }
    return EXCELLENT;

}






