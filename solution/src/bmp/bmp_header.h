//
// Created by Matthew on 23.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_HEADER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_HEADER_H
#include "../image/image.h"
#include "stdbool.h"
#include "stdio.h"

#define MYLAB3_BMP_HEADER_H
#pragma pack(push, 1)
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


#pragma pack(pop)

uint8_t calculate_padding(size_t width);
bool read_header (FILE *const file , struct bmp_header *const header);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_HEADER_H
