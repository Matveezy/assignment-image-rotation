//
// Created by Matthew on 25.12.2021.
//
#include "bmp_formatter.h"
//#include "bmp_writer.h"
//#include "bmp_header.h"


enum state create_header(struct image *const img, struct bmp_header *header);

enum state from_bmp(FILE *input, struct image *const img) {

    struct bmp_header header;
    if (!read_header(input, &header)) return READ_ERROR;


    const size_t src_width = header.biWidth;
    const size_t src_height = header.biHeight;
    *img = create_image(src_width, src_height);

    const uint8_t padding = calculate_padding(header.biWidth);

    for (size_t i = 0; i < src_height; i++) {
        void *start = img->data + img->width * i;

        fread(start, sizeof(struct pixel), src_width, input);
        fseek(input, padding, SEEK_CUR);
    }

    return EXCELLENT;
}

enum state from_array_to_bmp(FILE *output, struct image *const img) {
    struct bmp_header out_header;
    create_header(img, &out_header);

    if (!fwrite(&out_header, sizeof(struct bmp_header), 1, output)) return WRITE_ERROR;
    fseek(output, out_header.bOffBits, SEEK_SET);

    const size_t padding = calculate_padding(img->width);

    uint8_t zero = 0;

    if (img->data) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + img->width * i, img->width * sizeof(struct pixel), 1, output);
            for (size_t j = 0; j < padding; j++) {
                fwrite(&zero, 1, 1, output);
            }
        }
    } else return WRITE_ERROR;


    return EXCELLENT;

}
