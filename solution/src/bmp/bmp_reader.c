#include "bmp_header.h"
#include "stdbool.h"
#include "stdio.h"

bool read_header (FILE *const file , struct bmp_header *const header){
    return fread(header, sizeof(struct bmp_header) , 1 , file);
}

uint8_t calculate_padding(size_t width){
    if (width % 4 == 0 ) return 0;
    return 4 - (width * sizeof (struct pixel) % 4);
}


