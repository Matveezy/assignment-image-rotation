//
// Created by Matthew on 25.12.2021.
//

#include "bmp_writer.h"
#include "bmp_header.h"
//#include "../file_manager/file_manager.h"


/**
 * Рассчитываем размер картинки(только картинки): ширину умножаем на размер одного пикселя, плюс отступ padding.Далее все умножаем на количество таких строк
 * @param width
 * @param height
 * @param padding
 * @return
 */
static size_t get_image_size(size_t width, size_t height, size_t padding) {
    return (width * sizeof(struct pixel) + padding) * height;
}

/**
 * Рассчитываем размер файла (размер картинки + заголовки)
 * @param img_size
 * @return
 */
static size_t get_file_size(uint32_t img_size) {
    return img_size + sizeof(struct bmp_header);
}

enum state create_header(struct image *const img, struct bmp_header *header) {
    const size_t padding = calculate_padding(img->width);
    //  BitMap file Format
    header->bfType = 19778;

    header->bfileSize = get_file_size(get_image_size(img->width, img->height, padding));

    //    RESERVED - Зарезервированы и должны быть нулями
    header->bfReserved = 0;

    header->bOffBits = 54;


    header->biSize = 40;

    header->biWidth = img->width;
    header->biHeight = img->height;

    // PLANES - количество плоскостей
    header->biPlanes = 1;

    // BIT_COUNT - кол-во бит на один пиксель (у нас структура из 3 байтовых цветов => один пиксель 24 бита)
    header->biBitCount = 24;

    //    COMPRESSION - Тип сжатия
    header->biCompression = 0;

    // Размер изображения(только изображения)
    header->biSizeImage = get_image_size(img->width, img->height, padding);

    // PIXEL_PER_M - обозначают соответственно горизонтальное и
    // вертикальное разрешение (в пикселях на метр) конечного устройства, на которое будет выводиться битовый массив (растр).
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;

    //COLOR_USED - определяет количество используемых цветов из таблицы.
    //Если это значение равно нулю, то в растре используется максимально возможное количество цветов, которые разрешены значением biBitCount.

    header->biClrUsed = 0;

    //COLORS_IMPORTANT - это количество важных цветов. Определяет число цветов, которые необходимы для того, чтобы изобразить рисунок.
    //Если это значение равно 0 (как это обычно и бывает), то все цвета считаются важными.
    header->biClrImportant = 0;

    return EXCELLENT;
}
