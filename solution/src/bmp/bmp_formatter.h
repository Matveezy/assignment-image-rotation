//
// Created by Matthew on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_FORMATTER_H
#include "../file_manager/file_manager.h"
#include "bmp_header.h"
#include "stdbool.h"
#include "stdio.h"
#define ASSIGNMENT_IMAGE_ROTATION_BMP_FORMATTER_H
        enum state from_bmp(FILE *input, struct image *const img);
        enum state from_array_to_bmp(FILE* output , struct image* const img);
#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_FORMATTER_H
