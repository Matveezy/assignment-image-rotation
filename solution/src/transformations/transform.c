//
// Created by Matthew on 25.12.2021.
//

#include "transform.h"
#include "../image/image.h"
#include "rotation_90.h"

/**
 * Более абстрактный метод, который принимает в качестве аргумента угол поворота
 * @param image
 * @param transform
 * @return
 */
struct image transform(const struct image *image, enum param transform) {
    if (!image->data) {
        return (struct image) {.data=NULL};
    }

    struct pixel *data = malloc(sizeof(struct pixel) * image->width * image->height);
    switch (transform) {
        case (ROTATE_90_CLOCKWISE):
            return rotate_90_clockwise(image, data);

        case (ROTATE_90_COUNTERCLOCK):
            return rotate_90_counter_clock_wise(image, data);
        default:
            for (size_t i = 0; i < image->width * image->height; i++) {
                data[i] = image->data[i];
            }
            return (struct image) {.data = data, .height = image->height, .width = image->width};
    }
}

