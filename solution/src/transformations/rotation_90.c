//
// Created by Matthew on 25.12.2021.
//

#include "rotation_90.h"

/**
 * Метод, который переворачивает изображение на 90 градусов против часовой стрелки
 * @param image
 * @param data
 * @return
 */
struct image rotate_90_counter_clock_wise(const struct image *image, struct pixel* const data) {

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {

            data[image->height * j + (image->height - 1 - i)] = image->data[i * image->width + j];
        }
    }
    return (struct image) {.width = image->height, .height = image->width, .data = data};
}

/**
 * Метод , который переворачивает ихображение на 90 градусов по часовой стрелке
 * @param image
 * @param data
 * @return
 */
struct image rotate_90_clockwise(const struct image *image,struct pixel* const data) {
    for (size_t i = image->width; i > 0; --i) {
        for (size_t j = 0; j < image->height; j++) {
            data[j + (image->height * (image->width - i))] = image->data[(i - 1) + j * (image->width)];
        }
    }
    return (struct image) {.width = image->height, .height = image->width, .data = data};

}
