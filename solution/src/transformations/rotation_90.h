//
// Created by Matthew on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_90_H
#include "../image/image.h"
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_90_H
struct image rotate_90_clockwise(const struct image *image, struct pixel* const data);

struct image rotate_90_counter_clock_wise(const struct image *image, struct pixel* const data);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATION_90_H
