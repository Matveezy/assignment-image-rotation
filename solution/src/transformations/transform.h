//
// Created by Matthew on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

enum param {
    ROTATE_90_CLOCKWISE,
    ROTATE_90_COUNTERCLOCK
};

struct image transform(const struct image *image, enum param transform);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
