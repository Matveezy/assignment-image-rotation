#include "bmp/bmp_header.h"
#include "bmp/bmp_formatter.h"
#include "bmp/bmp_writer.h"
#include "file_manager/file_manager.h"
#include "image/image.h"
#include "transformations/transform.h"


enum state create_header(struct image *const img, struct bmp_header *header);

int main(int argc, char **argv) {
    /**
     * Проверяем кол-во введенных аргументов
     */
    if (argc != 3) {
        printf("Pass all arguments that required!\n");
        return 0;
    }

    /**
     * Путь исходного файла
     */
    const char *input_file_name = argv[1];

    /**
     * Путь к файлу-результату
     */
    const char *output_file_name = argv[2];

    /**
     * Открываем поток c файлом и присваиваем ссылку на него(файл) полю структуры
     */
    struct file input = file_open(input_file_name);
    FILE *input_file = input.file;


    if (input.state == EXCELLENT) {
        /**
         * Если чтение из файла произошло успешно,то переводим изображение из файла в структуру image и закрываем поток
         * , иначе выкидываем ошибку чтения из файла
         */
        struct image first_img;
        if (from_bmp(input.file, &first_img) == EXCELLENT) {
            file_close(input_file);

            /**
             * Создаем объект для новой(перевернутой) картинки, переворачиваем и записываем в новую ,чистим аллоцированную память старой
             */
            struct image transformed;
            transformed = transform(&first_img, ROTATE_90_COUNTERCLOCK);
            image_delete(first_img);

            /**
             * Открываем поток файла-результата и переводим перевернутую картинку в него, после чего закрываем поток и чистим память,
             * аллоцированную под картинку
             */
            struct file result = file_output_open(output_file_name);
            FILE *output = result.file;
            from_array_to_bmp(output, &transformed);
            file_close(output);
            image_delete(transformed);

            return 0;
        } else return CONVERT_ERROR;

    } else return READ_ERROR;


}
